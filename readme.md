#### Laravel API
##### Laravel API only project
###### [Online Version](http://test3.saasc.uk/ "Online Version")

Users of this App can check VAT via VIES VAT API trough this middle API and need Client ID & Secret to communicate with the service. Client secret will use to Verify Signature. We'll compare X-Signature header with Hmac. You should send this via header, for example: to post this `...//api/check?client_id=159753&country=FR&vat=82542065479` you should add this header to it: `'X-Signature' => $this->calculateHmac('...//api/check?client_id=159753&country=FR&vat=82542065479')` and calculateHmac method can be like this: `$data = strtok($url, "?"); base64_encode(hash_hmac('sha1', $data, OlyCapSir995!, true));`

**PHP Sample for creation X-Signature header:**

```php
$url = 'http://test3.saasc.uk/api/check?client_id=159753&country=FR&vat=82542065479';
$data = strtok($url, '?');
$hmac = base64_encode(hash_hmac('sha1', $data, 'OlyCapSir995!', true));
echo $hmac;
```

**cURL sample via PostMan codes:**

```curl
curl -X POST \
  'http://test3.saasc.uk/api/check?client_id=159753&country=FR&vat=82542065479' \
  -H 'Accept: */*' \
  -H 'Accept-Encoding: gzip, deflate' \
  -H 'Cache-Control: no-cache' \
  -H 'Connection: keep-alive' \
  -H 'Content-Length: ' \
  -H 'Host: test3.saasc.uk' \
  -H 'Postman-Token: 3bfe043c-18cf-4a93-a0f4-4493a0327a24,bfc319c7-c9fb-419c-850b-4e159ac327a5' \
  -H 'User-Agent: PostmanRuntime/7.15.2' \
  -H 'X-Signature: 5tosZr2FO5zNWzj/oR91M+vRlCk=' \
  -H 'cache-control: no-cache'
```

------------

**THE PURPOSE OF THE SERVICE:**

+ Handles all CRUD processes
    + Use `.../api/check` and `POST` --> `CheckController@check` for validating VAT with 

------------

**HOW TO INSTALL:**

+ Check for these: [PHP](http://php.net/manual/en/install.php "PHP") >= 7.1.3 & [common packages](https://laravel.com/docs/5.8#installation "common packages"), [Composer](https://getcomposer.org/doc/faqs/how-to-install-composer-programmatically.md "Composer"), [Node.js](https://nodejs.org/en/ "Node.js") & [NPM](https://www.npmjs.com/get-npm "NPM") and setup if needed
+ Also you need *SOAPClient PHP Extension enabled* that can be installed with `apt-get install php-soap`

+ Make sure about some permission like storage and cache
```bash
sudo chmod -R 775 storage
sudo chmod -R 775 bootstrap/cache
```

+ Make change related your requirements in .env.prod file & make a copy to the .env

+ Run Composer Install
```bash
composer install --optimize-autoloader --no-dev
```

+ Generate a new application encyption key in .env
```bash
php artisan key:generate
```

+ Make Tables in MySQL DB (`php artisan migrate --pretend` to see raw MySQL commands)
```bash
php artisan migrate
```

+ Cache config
```bash
php artisan config:cache
```