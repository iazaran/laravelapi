<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>VIES API</title>

        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="{{ asset('public/css/app.css') }}" rel="stylesheet">
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }
            .full-height {
                height: 100vh;
            }
            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }
            .position-ref {
                position: relative;
            }
            .content {
                text-align: center;
                max-width: 530px;
            }
            .title {
                font-size: 84px;
                letter-spacing: 1rem;
            }
            .m-b-md {
                margin-bottom: 30px;
            }
            .form-group {
                margin: 1rem;
            }
        </style>
        
        <script src="{{ asset('public/js/app.js') }}"></script>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">VIES API</div>
                <form>
                    <div class="form-group">
                        <label for="country">Select Country</label>
                        <select id="country" name="country" class="form-control">
                            <option value="">--</option>
                            <option value="AT">AT-Austria</option>
                            <option value="BE">BE-Belgium</option>
                            <option value="BG">BG-Bulgaria</option>
                            <option value="CY">CY-Cyprus</option>
                            <option value="CZ">CZ-Czech Republic</option>
                            <option value="DE">DE-Germany</option>
                            <option value="DK">DK-Denmark</option>
                            <option value="EE">EE-Estonia</option>
                            <option value="EL">EL-Greece</option>
                            <option value="ES">ES-Spain</option>
                            <option value="FI">FI-Finland</option>
                            <option value="FR">FR-France </option>
                            <option value="GB">GB-United Kingdom</option>
                            <option value="HR">HR-Croatia</option>
                            <option value="HU">HU-Hungary</option>
                            <option value="IE">IE-Ireland</option>
                            <option value="IT">IT-Italy</option>
                            <option value="LT">LT-Lithuania</option>
                            <option value="LU">LU-Luxembourg</option>
                            <option value="LV">LV-Latvia</option>
                            <option value="MT">MT-Malta</option>
                            <option value="NL">NL-The Netherlands</option>
                            <option value="PL">PL-Poland</option>
                            <option value="PT">PT-Portugal</option>
                            <option value="RO">RO-Romania</option>
                            <option value="SE">SE-Sweden</option>
                            <option value="SI">SI-Slovenia</option>
                            <option value="SK">SK-Slovakia</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="vat">VAT Number</label>
                        <input id="vat" name="vat" class="form-control" type="text">
                    </div>
                    <a id="check" class="btn btn-primary my-4 text-white" role="button">Check</a>
                </form>
                <div class="container my-4 lead border border-secondary rounded bg-light" id="result"></div>
                <div class="container my-4">This form is a test & not connected with DB. To use API see ReadMe at <a href="https://bitbucket.org/iazaran/laravelapi" target="_blank" class="text-dark text-decoration-none font-weight-normal">here</a>!</div>
            </div>
        </div>
    </body>
    <script type="text/javascript">
        $('#check').on('click',function(){
            $.ajax({
                type : 'get',
                url : '{{URL::to('publicCheck')}}',
                data:{'country': $('#country').val(), 'vat': $('#vat').val()},
                success:function(data){
                    if (data != '') {
                        $('#result').html(data);
                    }
                }
            });
        });
        $.ajaxSetup({ headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } });
    </script>
</html>