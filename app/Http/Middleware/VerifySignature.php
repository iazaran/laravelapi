<?php

namespace App\Http\Middleware;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

use App\Customer;

use Closure;

/**
 * VerifySignature Middleware is responsible for
 * verifying X-Signature header of the request.
 */
class VerifySignature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (empty($request->client_id)) {
            // Abort empty client_id requets
            abort(400);
        } elseif (!DB::table('customers')->where('client_id', $request->client_id)->exists()) {
            // Abort wrong client_id requets
            abort(400);
        } elseif (!$this->verifySignature($request)) {
            // Abort unauthorized requets
            abort(401);
        } else {
            return $next($request);
        }
    }

    /**
     * Verifies given requests signature
     * @param  \Illuminate\Http\Request $request received request
     * @return bool True if it is validated, false otherwise
     */
    private function verifySignature($request) :bool
    {
        return ($request->header('X-Signature') == $this->calculateHmac($request));
    }

    /**
     * Calculates HMAC X-Signature value of the given request
     * @param  \Illuminate\Http\Request $request given request
     * @return string HMAC SHA1 value of the request
     */
    private function calculateHmac($request)
    {
        // Split params from the full URL
        $data = strtok($request->fullUrl(), '?');

        // Use related client_secret to verification header
        $customerData = Customer::OfSecret($request->client_id)->first();
        $hmac = base64_encode(hash_hmac('sha1', $data, $customerData->client_secret, true));
        
        Log::debug(['url' => $request->fullUrl(), 'data' => $data, 'X-Signature' => $request->header('X-Signature'), 'hmac' => $hmac]);

        return $hmac;
    }
}
