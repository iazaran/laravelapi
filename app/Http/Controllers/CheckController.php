<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

use App\Vat;
use App\Customer;
use App\Services\ClientUtil;

class CheckController extends Controller
{
    /**
     * Create a new CheckController instance
     * @return void
     */
    public function __construct()
    {
        $this->middleware('cors', []);

        // Make public access for the test in homepage
        $this->middleware('verifySignature', [])->except('publicCheck');
    }

    /**
     * VAT validation via it's SDK for our customers
     * Customers can use simple POST method with
     * empty body and send parameters:
     * client_id, country & vat
     * with hmac header
     * See here: https://bitbucket.org/iazaran/laravelapi
     *
     * @param Request $request
     * @return JSON array
     */
    public function check(Request $request)
    {
        if (empty($request->country) || empty($request->vat)) {
            return response()->json(['error' => 'Empty parameter(s)!'], 400);
        } elseif (strlen($request->country) != 2) {
            return response()->json(['error' => 'Wrong country code!'], 400);
        }
        Log::debug(['request' => $request]);

        // Make an array from all data
        $customerData = Customer::OfSecret($request->client_id)->first();
        $data = array();
        $data['customer_id'] = $customerData->id;
        $data['country'] = $request->country;
        $data['vat'] = $request->vat;

        // Make a call to VIES and get response via ClientUtil service
        $response = ClientUtil::api_call($data);

        Log::debug(['response' => $response]);
        $data['name'] = $response['name'];
        $data['address'] = $response['address'];
        $data['name'] = $response['is_valid'];
        Vat::create($data);

        return response()->json($response, 200);
    }

    /**
     * Same as check, public for access via homepage for the test
     *
     * @param Request $request
     * @return string
     */
    public function publicCheck(Request $request)
    {
        if($request->ajax()) {
            $output = '';
            if (empty($request->country) || empty($request->vat)) {
                $output = 'Empty parameter(s)!';
            } elseif (strlen($request->country) != 2) {
                $output = 'Wrong country code!';
            } else {
                Log::debug(['request' => $request]);

                // Make an array from all data
                $data = array();
                $data['customer_id'] = '';
                $data['country'] = $request->country;
                $data['vat'] = $request->vat;
                
                // Make a call to VIES and get response via ClientUtil service
                $response = ClientUtil::api_call($data);

                Log::debug(['response' => $response]);

                if ($response['is_valid'] == 0) {
                    $output = '<strong>' . strtoupper($response['country']) . ' ' . $response['vat'] . '</strong> is not valid!';
                } else {
                    $output = '<strong>' . strtoupper($response['country']) . ' ' . $response['vat'] . '</strong> is valid. <br/> Address: ' . $response['address'] . ' <br/> Name: ' . $response['name'];
                }
            }

            return Response($output);
        }
    }
}
