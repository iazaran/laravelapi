<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'client_id', 'client_secret'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = ['client_secret'];

    /**
     * Scope a query to only include data of a given client_id
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $client_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfSecret($query, $client_id)
    {
        return $query->where('client_id', $client_id);
    }

    // Each customer related with many vat processes
    public function vats()
    {
        return $this->hasMany('App\Vat');
    }
}
