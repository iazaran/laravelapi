<?php
namespace App\Services;

use Illuminate\Support\Facades\Log;

use App\Services\Validator;
use App\Services\Provider\Europa;
use App\Services\Exception as EuVatExcept;

/**
 * A Utility class for VIES calls
 */
class ClientUtil
{
    /**
     * Call VIES & get response
     *
     * @param array $data
     * @return array
     */
    public static function api_call($data = [])
    {
        $result = array();
        $result['country'] = $data['country'];
        $result['vat'] = $data['vat'];
        $result['name'] = '';
        $result['address'] = '';
        Log::debug(['Client util request' => $data]);
        try {
            $oVatValidator = new Validator(new Europa, $result['vat'], $result['country']);
            if ($oVatValidator->check()) {
                $result['name'] = $oVatValidator->getName();
                $result['address'] = $oVatValidator->getAddress();
                $result['is_valid'] = 1;
            } else {
                $result['is_valid'] = 0;
            }
            return $result;
        } catch (EuVatExcept $oExcept) {
            echo ' Service unavailable! ';
        }
    }
}
