<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vat extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'vat', 'country', 'address', 'name', 'is_valid'
    ];

    /**
     * Scope a query to only include data of a given customer id
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @param  string  $customer_id
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfCustomer($query, $customer_id)
    {
        return $query->where('customer_id', $customer_id);
    }

    // Each vat process related with a customer
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
